<?php

/**
 * YIID Admin settings form.
 */
function yiid_admin_form() {
  $form['node_types']['#tree'] = TRUE;
  foreach (node_get_types() as $k => $node_type) {   
    $form['node_types'][$k]['name'] = array(
      '#type' => 'item',
      '#value' => $node_type->name
    );

    $form['node_types'][$k]['position'] = array(
      '#type' => 'radios',
      '#options' => array(
        YIID_STYLE_POSITION_NONE => t('None'),
        YIID_STYLE_POSITION_LINK => t('Link area'),
        YIID_STYLE_POSITION_HEADER => t('Node header'),
        YIID_STYLE_POSITION_FOOTER => t('Node footer'),
      ),
      '#default_value' => variable_get("yiid_node_{$k}_position", YIID_NODE_POSITION_DEFAULT),
    );

    $form['node_types'][$k]['style'] = array(
      '#type' => 'radios',
      '#options' => yiid_style_options(),
      '#default_value' => variable_get("yiid_node_{$k}_style", YIID_NODE_STYLE_DEFAULT),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Theming YIID admin settings form.
 */
function theme_yiid_admin_form(&$form) {
  foreach (element_children($form['node_types']) as $k) {
    $rows[] = array(
      drupal_render($form['node_types'][$k]['name']),
      drupal_render($form['node_types'][$k]['position']),
      drupal_render($form['node_types'][$k]['style']),
    );
  }

  $header = array(
    t('Node type'),
    t('Display Position'),
    t('Style'),
  );

  $output = theme('table', $header, $rows);

  return $output . drupal_render($form);
}

/**
 * Submit handler for YIID admin settings form.
 */
function yiid_admin_form_submit(&$form, &$form_state) {
  foreach ($form_state['values']['node_types'] as $k => $node_type_settings) {
    variable_set("yiid_node_{$k}_position", $node_type_settings['position']);
    variable_set("yiid_node_{$k}_style", $node_type_settings['style']);
  }
}
